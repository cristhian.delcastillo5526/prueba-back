using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Proyecto_3.Data_Base;
using Proyecto_3.Services.Implementations;
using AutoMapper;

namespace Proyecto_3
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer("Server=localhost;Database=StoreDCA;Integrated Security=True;MultipleActiveResultSets=true;") // Cadena de conexion a base de datos
            );  // despues realizamos el comando desde las herrramientas nuget     add-migration usuariosYadminn -o DataBase/Migrations
            services.AddControllers();
            services.AddScoped<IProductServices, ProductServices>();
            services.AddScoped<IUsuarioServices, UsuarioServices>();
            services.AddScoped<IComprasServices, ComprasServices>();

            services.AddAutoMapper(typeof(Startup));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(options => { options.AllowAnyOrigin(); options.AllowAnyHeader(); options.AllowAnyMethod(); });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            InitializeDataBase(app); // mandar llamar migracion de base de datos
        }

        public void InitializeDataBase(IApplicationBuilder app) {
        
            using(var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope()) 
            { 
                scope.ServiceProvider.GetRequiredService<AppDbContext>().Database.Migrate(); 
            }          // tomamos los datos de la base de datos del codigo c# y lo migramos
        
        }
    }

    
}
