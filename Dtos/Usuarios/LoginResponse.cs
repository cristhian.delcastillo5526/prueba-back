﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Dtos.Usuarios
{
    public class LoginResponse
    {

        public int UsuarioId { get; set; }

        public string Correo { get; set; }

        public bool Administrador { get; set; }

    }
}
