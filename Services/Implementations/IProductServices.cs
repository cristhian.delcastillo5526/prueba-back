﻿using Microsoft.AspNetCore.Http;
using Proyecto_3.Data_Base.Entities;
using Proyecto_3.DataTransferObjets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Services.Implementations
{
    public interface IProductServices
    {
        public IEnumerable<Product> GetAll();

        public IEnumerable<Product> Get(string name, decimal? precio, bool? status, int? typeProduct);

        public Product GetByID(int id);


        public IEnumerable<Product> GetAllType(int typeProduct);

        public Product Create(CreateProductRequest createProductRequest);

        // public IEnumerable<string> GetByName(string name);

        public bool Delete(int id);

        //parte de guardar imagen
        public Product Update(int id, UpdateProductRequest updateProductRequest);

        //PAra guardar la imagen
        public Task<string> SaveImage(int id, IFormFile file);
    }
}

//Se hace primero