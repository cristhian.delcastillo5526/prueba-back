﻿using Proyecto_3.DataBase.Entities;
using Proyecto_3.DataTransferObjets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Services.Implementations
{
    public interface IComprasServices
    {
        public Compras Create(CreateComprasRequest createComprasRequest);

        public Compras GetByID(int BuyId);
        public IEnumerable<Compras> GetAll();

        public IEnumerable<Compras> Get(int usuarioId, int statusCompra);

    }

}
