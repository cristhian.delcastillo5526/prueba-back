﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Proyecto_3.Data_Base;
using Proyecto_3.Data_Base.Entities;
using Proyecto_3.DataBase.Entities;
using Proyecto_3.DataTransferObjets;
using Proyecto_3.Dtos.Usuarios;

namespace Proyecto_3.Services.Implementations
{
    public class UsuarioServices : IUsuarioServices
    {
        private readonly AppDbContext _appDbContext;
        private readonly IMapper _mapper;


        public UsuarioServices(AppDbContext appDbContext, IMapper mapper) 
        {

            _appDbContext = appDbContext;
            _mapper = mapper;


        }

        public IEnumerable<Usuarios> GetAll()
        {

            return _appDbContext.Usuarios.ToList();
        }


        public LoginResponse Login(CreateUsuarioRequest createUsuarioRequest)
        {
            var Usuarios =_appDbContext.Usuarios.FirstOrDefault
                  (p => p.Contrasena == createUsuarioRequest.Contrasena
                  && p.Correo == createUsuarioRequest.Correo);

            var Response = _mapper.Map<LoginResponse>(Usuarios);
            return Response;       
        }


        public bool LoginAdmin(CreateUsuarioRequest createUsuarioRequest)
        {


            var usuarioLogin = _appDbContext.Usuarios.FirstOrDefault
                  (p => p.Contrasena == createUsuarioRequest.Contrasena
                  && p.Correo == createUsuarioRequest.Correo);



            if (usuarioLogin != null && usuarioLogin.Administrador == true)
            {
                return true;
            }

            return false;


        }


        public Usuarios Create(CreateUsuarioRequest createUsuarioRequest)
        {


            var usuario = new Usuarios
            {
                Correo = createUsuarioRequest.Correo,
                Contrasena = createUsuarioRequest.Contrasena,
                Nombre = createUsuarioRequest.Nombre,
                Domicilio = createUsuarioRequest.Domicilio,
                Administrador = createUsuarioRequest.Administrador,
                Telefono = createUsuarioRequest.Telefono

            };

            _appDbContext.Usuarios.Add(usuario);
            _appDbContext.SaveChanges();

            return usuario;

        }

        public IEnumerable<Usuarios> Get(int? UsuarioId, string Correo, string Contrasena, bool? Administrador, int? Telefono, string Nombre, string Domicilio)
        {
            var Usuarios = _appDbContext.Usuarios.AsQueryable(); // lista consultable

            if(UsuarioId != null)
            {
                Usuarios = Usuarios.Where(p => p.UsuarioId == UsuarioId);
            }

            if (!string.IsNullOrWhiteSpace(Correo)) 
            {
                Usuarios = Usuarios.Where(p => p.Correo.Contains(Correo));
            }

            if (!string.IsNullOrWhiteSpace(Contrasena))
            {
                Usuarios = Usuarios.Where(p => p.Contrasena.Contains(Contrasena));
            }

            if (Administrador != null)
            {
                Usuarios = Usuarios.Where(p => p.Administrador == Administrador);
            }

            if (!string.IsNullOrWhiteSpace(Nombre))
            {
                Usuarios = Usuarios.Where(p => p.Nombre == Nombre);
            }

            if (Telefono != null)
            {
                Usuarios = Usuarios.Where(p => p.Telefono == Telefono);
            }

            if (!string.IsNullOrWhiteSpace(Domicilio))
            {
                Usuarios = Usuarios.Where(p => p.Domicilio == Domicilio);
            }

            return Usuarios.ToList();
        }

        public Usuarios GetByID(int UsuarioId)
        {

            return _appDbContext.Usuarios.FirstOrDefault(m => m.UsuarioId == UsuarioId);

        }

        public Usuarios Update(int UsuarioId, UpdateUsuarioRequest updateUsuarioRequest)
        {

            Usuarios usuarios = GetByID(UsuarioId);
            usuarios.Administrador = updateUsuarioRequest.Administrador;
            usuarios.Contrasena = updateUsuarioRequest.Contrasena;
            usuarios.Correo = updateUsuarioRequest.Correo;
            usuarios.Domicilio = updateUsuarioRequest.Domicilio;
            usuarios.Nombre = updateUsuarioRequest.Nombre;
            usuarios.Telefono = updateUsuarioRequest.Telefono;

            _appDbContext.SaveChanges();

            return usuarios;

        }
        public bool Delete(int UsuarioId)
        {

            Usuarios usuario = GetByID(UsuarioId);
            _appDbContext.Usuarios.Remove(usuario);

            int records = _appDbContext.SaveChanges();

            //if (records > 0) {

            //    return true;
            //}
            //return false;

            return records > 0;


        }
    }
}
