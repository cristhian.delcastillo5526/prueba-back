﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Proyecto_3.Data_Base;
using Proyecto_3.Data_Base.Entities;
using Proyecto_3.DataTransferObjets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_3.Services.Implementations
{
    public class ProductServices : IProductServices
    {
        private readonly AppDbContext _appDbContext;
        public ProductServices(AppDbContext appDbContext) {

            _appDbContext = appDbContext;
        
        }

        //List<Product> products = new List<Product> {
        //    new Product {Id = 1, Name = "Cocacola", Precio = 15, Status = true },
        //    new Product {Id = 2, Name = "Frijol", Precio = 32, Status = true, Weight = 1000, 
        //                 BranchProducts = new List<BranchProduct>{ new BranchProduct {BranchId = 1, Quantity = 5}, new BranchProduct{BranchId = 2, Quantity = 3} } }                                        


        //};  // esto se llama moquear asignar valores a las variables que debe de venir de base de datos o algun otro lado

        //public IEnumerable<Product> GetAll()
        //{

        //    return products;
        //}


        public IEnumerable<Product> GetAll()
        {

            return _appDbContext.Products.ToList();
        }


        public IEnumerable<Product> Get(string name, decimal? precio, bool? status, int? typeProduct)
        {
            var products = _appDbContext.Products.AsQueryable(); // lista consultable

            if (!string.IsNullOrWhiteSpace(name))
            {
                products = products.Where(p => p.Name.Contains(name));
            }

            if (precio != null)
            {
                products = products.Where(p => p.Precio >= precio);
            }

            if (status != null)
            {
                products = products.Where(p => p.Status == status);
            }

            if (typeProduct != null)
            {
                products = products.Where(p => p.TypeProduct == typeProduct);
            }

            return products.ToList();


        }

        public Product GetByID(int id) {

            return _appDbContext.Products.FirstOrDefault(m => m.Id == id);
        
        }


        public IEnumerable<Product> GetAllType(int typeProduct)
        {

            return _appDbContext.Products.Where(m => m.TypeProduct == typeProduct).ToList();
        }

        public Product Create(CreateProductRequest createProductRequest)
        {

            var product = new Product
            {
                Name = createProductRequest.Name,
                Precio = createProductRequest.Precio,
                Status = createProductRequest.Status,
                Weight = createProductRequest.Weight,
                TypeProduct = createProductRequest.TypeProduct
            };


            _appDbContext.Products.Add(product);
            _appDbContext.SaveChanges();
            return product;

        }

        public bool Delete(int id)
        {

            Product product = GetByID(id);
            _appDbContext.Products.Remove(product);

            int records = _appDbContext.SaveChanges();

            //if (records > 0) {

            //    return true;
            //}
            //return false;

            return records > 0;


        }

        public Product Update(int id, UpdateProductRequest updateProductRequest)
        {

            Product product = GetByID(id);
            product.Precio = updateProductRequest.Precio;
            product.Status = updateProductRequest.Status;
            product.Name = updateProductRequest.Name;
            product.Weight = updateProductRequest.Weight;
            product.TypeProduct = updateProductRequest.TypeProduct;

            _appDbContext.SaveChanges();

            return product;

        }

        ////////////////////////////// estos metodos sirven para guardar un archivo ///////////////////////////////////////////
        public static void SaveBytesToFile(Byte[] byteArray, string fileName, string assetFolder = "./assets/")
        {
            FileStream fileStream = new FileStream(assetFolder + fileName, FileMode.Create, FileAccess.ReadWrite);
            fileStream.Write(byteArray, 0, byteArray.Length);
            fileStream.Close();
        }


        public async Task<string> SaveImage(int id, IFormFile file)
        {
            var item = await _appDbContext.Products.FirstOrDefaultAsync(i => i.Id == id);

            if (item is null)

                return null;

            if (file is null)

                return null;

            item.Imagen = Guid.NewGuid() + GetFileExtension(file);
            var fileBytes = GetFileBytes(file);
            SaveBytesToFile(fileBytes, item.Imagen);
            //if(_options.UseFtp)
            //    UploadFtpFile(item.ImageName, fileBytes);

            await _appDbContext.SaveChangesAsync();

            return item.Imagen;
        }


        public string GetFileExtension(IFormFile file)
        {
            return (file?.ContentType) switch
            {
                "image/jpeg" => ".jpeg",
                "image/png" => ".png",
                "image/tiff" => ".tiff",
                "image/svg+xml" => ".svg",
                _ => null,
            };
        }

        public byte[] GetFileBytes(IFormFile file)
        {
            var reader = new BinaryReader(file.OpenReadStream(), Encoding.UTF8);
            return reader.ReadBytes((int)file.Length);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
}
