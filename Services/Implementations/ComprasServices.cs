﻿using Proyecto_3.Data_Base;
using Proyecto_3.DataBase.Entities;
using Proyecto_3.DataTransferObjets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Services.Implementations
{
    public class ComprasServices: IComprasServices
    {
        private readonly AppDbContext _appDbContext;
        public ComprasServices(AppDbContext appDbContext)
        {

            _appDbContext = appDbContext;

        }

        public IEnumerable<Compras> Get(int usuarioId, int statusCompra)
        {
            var compras = _appDbContext.Compras.Where(p => p.UsuarioId==usuarioId && p.StatusCompra == statusCompra); // lista consultable
            


            return compras.ToList();


        }

        public IEnumerable<Compras> GetAll()
        {

            return _appDbContext.Compras.ToList();
        }

        public Compras Create(CreateComprasRequest createComprasRequest)
         {

            var compras = new Compras
            {
                BuyId = createComprasRequest.BuyId,
                Name = createComprasRequest.Name,
                Precio = createComprasRequest.Precio,
                TypeProduct = createComprasRequest.TypeProduct,
                Status = createComprasRequest.Status,
                Weight = createComprasRequest.Weight,
                UsuarioId = createComprasRequest.UsuarioId,
                StatusCompra = 0

           };
    

            _appDbContext.Compras.Add(compras);
           _appDbContext.SaveChanges();
           return compras;

        }


        public Compras GetByID(int BuyId)
        {

            return _appDbContext.Compras.FirstOrDefault(m => m.BuyId == BuyId);

        }




    }
}
