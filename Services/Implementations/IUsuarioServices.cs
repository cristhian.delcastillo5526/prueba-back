﻿using Proyecto_3.Data_Base.Entities;
using Proyecto_3.DataBase.Entities;
using Proyecto_3.DataTransferObjets;
using Proyecto_3.Dtos.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;




namespace Proyecto_3.Services.Implementations
{
    public interface IUsuarioServices
    {
        public IEnumerable<Usuarios> GetAll();
        public IEnumerable<Usuarios> Get(int? UsuarioId, string Correo, string Contrasena, bool? Administrador, int? Telefono, string Nombre, string Domicilio);
        public Usuarios Create(CreateUsuarioRequest createUsuarioRequest);

        public LoginResponse Login(CreateUsuarioRequest createUsuarioRequest);

        public bool LoginAdmin(CreateUsuarioRequest createUsuarioRequest);

        public bool Delete(int UsuarioId);

        public Usuarios GetByID(int UsuarioId);
        public Usuarios Update(int UsuarioId, UpdateUsuarioRequest updateUsuarioRequest);




    }
}
