﻿using Microsoft.EntityFrameworkCore;
using Proyecto_3.Data_Base.Entities;
using Proyecto_3.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Data_Base
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Branch> Branches { get; set; }
        public DbSet<BranchProduct> BranchProducts {get; set;}
        public DbSet<Product> Products { get; set;}
        public DbSet<Store> Stores { get; set; }
        public DbSet<Usuarios> Usuarios { get; set; }
        public DbSet<Compras> Compras { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)       // configura la conexion a base de datos
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)  // configura las entidades de la base de datos
        {
            modelBuilder.Entity<BranchProduct>().HasKey(P => new { P.BranchId, P.ProductId });   // LLave compuesta,  Depues nos vamos al star up a la cadena de conexion
            base.OnModelCreating(modelBuilder);
        }
    }
}
