﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Proyecto_3.DataBase.Migrations
{
    public partial class comprasDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DataCompra",
                table: "Compras",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StatusCompra",
                table: "Compras",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UsuarioId",
                table: "Compras",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataCompra",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "StatusCompra",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "UsuarioId",
                table: "Compras");
        }
    }
}
