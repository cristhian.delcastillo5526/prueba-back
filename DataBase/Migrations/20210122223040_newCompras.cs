﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Proyecto_3.DataBase.Migrations
{
    public partial class newCompras : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DirectionClient",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "NameClient",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "NameProduct",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "PriceProduct",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "Compras");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Compras",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Precio",
                table: "Compras",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Status",
                table: "Compras",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "TypeProduct",
                table: "Compras",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Weight",
                table: "Compras",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "Precio",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "TypeProduct",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "Weight",
                table: "Compras");

            migrationBuilder.AddColumn<string>(
                name: "DirectionClient",
                table: "Compras",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "NameClient",
                table: "Compras",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "NameProduct",
                table: "Compras",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "PriceProduct",
                table: "Compras",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Total",
                table: "Compras",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
