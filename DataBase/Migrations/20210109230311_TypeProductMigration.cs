﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Proyecto_3.DataBase.Migrations
{
    public partial class TypeProductMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TypeProduct",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TypeProduct",
                table: "Products");
        }
    }
}
