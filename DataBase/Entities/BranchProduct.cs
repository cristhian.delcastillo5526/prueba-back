﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Data_Base.Entities
{
    public class BranchProduct
    {
        // properties
        public int ProductId { get; set; }
        public int BranchId { get; set; }
        public decimal Price { get; set; }
        public bool Status { get; set; }
        public int Quantity { get; set; } // o Stock (cantidad)

        // navigation propieties 
        public virtual Product Product { get; set; }  // es un PUBLIC virtuAL POR CADA RELACION EN ESTE CASO EL iD
        public virtual Branch Branch { get; set; }
       
    }
}
