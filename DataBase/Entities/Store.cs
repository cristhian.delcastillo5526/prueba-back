﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Data_Base.Entities
{
    public class Store
    {   
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(100)]
        public string NameStore { get; set; }


        // Relacion de base de datos Para relacionar la tienda con todas sus sucursales es de : 1-N
        public virtual ICollection<Branch> Branches { get; set; }
    }
}
