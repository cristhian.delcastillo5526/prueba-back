﻿using Proyecto_3.Data_Base.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace Proyecto_3.DataBase.Entities
{
    public class Usuarios
    {
      
        // properties

        [Key]
        public int UsuarioId { get; set; }

        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        public string Correo { get; set; }

        [StringLength(100)]
        public string Nombre { get; set; }

        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        public string Contrasena { get; set; }

        public bool Administrador { get; set; }

        [StringLength(100)]
        public string Domicilio { get; set; }
        public int? Telefono { get; set; }


        // navigation propieties 
        // public virtual Product Product { get; set; }  // es un PUBLIC virtuAL POR CADA RELACION EN ESTE CASO EL iD
        // public virtual Branch Branch { get; set; }
    }
}
