﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Data_Base.Entities
{
    public class Branch  // branch es sucursal
    {
        public Branch() {
            BranchProducts = new HashSet<BranchProduct>(); // no registrar entidades duplicadas HashSet
        }  // constructore para constriur la entidad y todas las clases tienen un constructor vacio explicito o implicito

        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(100)]
        public string Address { get; set; } // Domicilio

        [StringLength(100)]
        public string Schedule { get; set; } //Horario

        [Required(AllowEmptyStrings = false)]
        [StringLength(100)]
        public string NameBranch { get; set; } //nombre de la sucursal

        public int StoreId { get; set; } 

        public virtual Store Store { get; set; } // Es para relacionar una sucursal con la unica tienda correspondiente de N-1
        public virtual ICollection<BranchProduct> BranchProducts { get; set; } // para varios productos

    }
}
