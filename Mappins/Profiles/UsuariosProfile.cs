﻿using AutoMapper;
using Proyecto_3.DataBase.Entities;
using Proyecto_3.Dtos.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Mappins.Profiles
{
    public class UsuariosProfile : Profile
    {
        public UsuariosProfile()
        {
            // CreateMap<Usuarios, LoginResponse>().ForMember(d => d.UsuarioId, opt => opt.MapFrom(src => src.UsuarioId)).ForMember(d => d.Correo, opt => opt.MapFrom(src => src.Correo)).ForMember(d => d.Administrador, opt => opt.MapFrom(src => src.Administrador));
            CreateMap<Usuarios, LoginResponse>(); // automatico y el de arriba es manual
        }
    }
}
