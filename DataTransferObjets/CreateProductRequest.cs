﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.DataTransferObjets
{
    public class CreateProductRequest
    {
      

        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        public decimal? Precio { get; set; }  // el precio puede ser no requiro y se guardaria como nulo con ?
        public bool Status { get; set; }
        public decimal? Weight { get; set; } // el signo representa si esa propuedad tiene valor o es nulo (nullables)
        public int? TypeProduct { get; set; }

    }
}
