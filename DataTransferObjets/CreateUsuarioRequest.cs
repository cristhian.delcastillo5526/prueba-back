﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.DataTransferObjets
{
    public class CreateUsuarioRequest
    {

        public int UsuarioId { get; set; }

        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        public string Correo { get; set; }

        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        public string Contrasena { get; set; }

        [StringLength(100)]
        public string Nombre { get; set; }

        public bool Administrador { get; set; }

        [StringLength(100)]
        public string Domicilio { get; set; }
        public int? Telefono { get; set; }

        // el precio puede ser no requiro y se guardaria como nulo con ?

    }
}
