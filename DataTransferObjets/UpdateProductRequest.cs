﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.DataTransferObjets
{
    public class UpdateProductRequest
    {
       
        public string Name { get; set; }
        public decimal Weight { get; set;}
        public decimal? Precio { get; set; }  // el precio puede ser no requiro y se guardaria como nulo con ?
        public bool Status { get; set; }
        public int? TypeProduct { get; set; }


    }
}
