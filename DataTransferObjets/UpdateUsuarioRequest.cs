﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.DataTransferObjets
{
    public class UpdateUsuarioRequest
    {
        //Request es solicitud
        public int UsuarioId { get; set; }

        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        public string Correo { get; set; }

        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        public string Nombre { get; set; }

        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        public string Contrasena { get; set; }

        public bool Administrador { get; set; }

        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        public string Domicilio { get; set; }
        public int? Telefono { get; set; }
    }
}
