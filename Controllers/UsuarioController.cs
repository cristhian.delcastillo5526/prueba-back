﻿using Microsoft.AspNetCore.Mvc;
using Proyecto_3.Data_Base.Entities;
using Proyecto_3.DataBase.Entities;
using Proyecto_3.DataTransferObjets;
using Proyecto_3.Dtos.Usuarios;
using Proyecto_3.Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Proyecto_3.Controllers
{
    [ApiController]

    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioServices _UsuarioService;


        public UsuarioController(IUsuarioServices usuarioServices)
        {
            _UsuarioService = usuarioServices;
        }

        [HttpGet]

        [Route("{UsuarioId}")]
        public Usuarios GetById(int UsuarioId)
        {

            return (_UsuarioService.GetByID(UsuarioId));

        }

        [HttpGet]
        public IEnumerable<Usuarios> GetAll()
        {

            return _UsuarioService.GetAll();

        }

        [HttpPost]
        public Usuarios Create([FromBody] CreateUsuarioRequest usuario)
        {

            return _UsuarioService.Create(usuario);

        }

        [HttpPost]
        [Route("login")]
        public LoginResponse Login([FromBody] CreateUsuarioRequest Usuario)
        {
            var usuario = _UsuarioService.Login(Usuario);

            return usuario;

        }

        [HttpPut]
        [Route("{UsuarId}")]

        public Usuarios Update(int UsuarId, [FromBody] UpdateUsuarioRequest updateUsuarioRequest)
        {

            return _UsuarioService.Update(UsuarId, updateUsuarioRequest);

        }

        [HttpDelete]
        [Route("{UsuarioId}")]

        public bool Delete(int UsuarioId)
        {

            return _UsuarioService.Delete(UsuarioId);
        }
    }
}