﻿using Microsoft.AspNetCore.Mvc;
using Proyecto_3.DataBase.Entities;
using Proyecto_3.DataTransferObjets;
using Proyecto_3.Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_3.Controllers
{
    [ApiController]

    [Route("[controller]")]
    public class ComprasController : ControllerBase
    {
        private readonly IComprasServices _comprasService;

        public ComprasController(IComprasServices comprasServices)
        {

            _comprasService = comprasServices;
        }

        [HttpPost]
        public Compras Create([FromBody] CreateComprasRequest compras)
        {

            return _comprasService.Create(compras);

        }

        [HttpGet]

        [Route("{BuyId}")]
        public Compras GetById(int BuyId)
        {

            return (_comprasService.GetByID(BuyId));

        }

        [HttpGet]
        [Route("filter/{usuarioId}/{statusCompra}")]
        public IEnumerable<Compras> Get(int usuarioId, int statusCompra)
        {
            return _comprasService.Get(usuarioId, statusCompra);

        }

        [HttpGet]
        public IEnumerable<Compras> GetAll()
        {

            return (_comprasService.GetAll());
        }
    }
}
