﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Proyecto_3.Data_Base.Entities;
using Proyecto_3.DataTransferObjets;
using Proyecto_3.Services.Implementations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Proyecto_3.Controllers // nombre de mi proyecto y clase
{
    [ApiController]

    [Route("[controller]")]
    public class ProductController : ControllerBase  //Obligatoriamente (Controller)
    {
        private readonly IProductServices _productService;

        public ProductController(IProductServices productServices)
        {

            _productService = productServices;
        }

        [HttpGet]
        public IEnumerable<Product> GetAll()
        { 

            return (_productService.GetAll());
        }

        [HttpGet]

        [Route ("{id}")]
        public Product GetById(int id)
        {

            return (_productService.GetByID(id));

        }


        [HttpGet]

        [Route("type/{typeProduct}")]
        public IEnumerable<Product> GetAllType(int typeProduct)
        {

            return (_productService.GetAllType(typeProduct));
        }


        [HttpGet("imagen/{name}")]
        public IActionResult Get(string name)
        {

                Byte[] b = System.IO.File.ReadAllBytes(@"./assets/" + name);
                return File(b, "image/jpeg");
            
        }



        [HttpPost]
        public Product Create([FromBody] CreateProductRequest product) {

             return _productService.Create(product);

        }

        [HttpDelete]
        [Route("{id}")]

        public bool Delete(int id) {

            return _productService.Delete(id);
        }

        [HttpPut]
        [Route("{id}")]

        public Product Update(int id, [FromBody] UpdateProductRequest updateProductRequest)
        {

            return _productService.Update(id, updateProductRequest);

        }

        [HttpGet]
        [Route("filter")]
        public IEnumerable<Product> Get([FromQuery]string name, [FromQuery] decimal? precio, [FromQuery] bool? status, [FromQuery] int? typeProduct)
        {
            return _productService.Get(name, precio, status, typeProduct);

        }

        //guardar imagen mediante el id
        [HttpPut]
        [Route("put-Imagen/{id}")]

        public string Update(int id)
        {
            IFormFile file = null;
            if (Request.Form.Files == null || !Request.Form.Files.Any())
                return null;
            file = Request.Form.Files[0];
            var result = _productService.SaveImage(id, file).Result;
            return result;
        }
    }
}



// Se crea al ultimo